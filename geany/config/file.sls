# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import mapdata as geany with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_package_install }}

{% if salt['pillar.get']('geany-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_geany', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

geany-config-file-user-{{ name }}-present:
  user.present:
    - name: {{ name }}

geany-config-file-geany-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/geany
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - geany-config-file-user-{{ name }}-present

geany-config-file-geany-config-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/geany/geany.conf
    - source: {{ files_switch([
                  name ~ '-geany.conf.tmpl',
                  'geany.conf.tmpl'],
                lookup='geany-config-file-geany-config-' ~ name ~ '-managed',
                )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - geany-config-file-geany-dir-{{ name }}-managed

geany-config-file-geany-keybindings-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/geany/keybindings.conf
    - source: {{ files_switch([
                  name ~ '-keybindings.conf.tmpl',
                  'keybindings.conf.tmpl'],
                lookup='geany-config-file-geany-keybindings-' ~ name ~ '-managed',
                )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - geany-config-file-geany-dir-{{ name }}-managed

geany-config-file-geany-ui-toolbar-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/geany/ui_toolbar.xml
    - source: {{ files_switch([
                  name ~ '-ui_toolbar.xml.tmpl',
                  'ui_toolbar.xml.tmpl'],
                lookup='geany-config-file-geany-ui-toolbar-' ~ name ~ '-managed',
                )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - geany-config-file-geany-dir-{{ name }}-managed
{% endif %}
{% endfor %}
{% endif %}
