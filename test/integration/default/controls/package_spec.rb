# frozen_string_literal: true

packages = %w[geany geany-plugins]

packages.each do |pkg|
  control "geany-package-install-#{pkg}-pkg-installed" do
    title 'should be installed'

    describe package(pkg) do
      it { should be_installed }
    end
  end
end
