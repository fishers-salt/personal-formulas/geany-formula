# frozen_string_literal: true

control 'geany-config-file-user-auser-present' do
  title 'should be present'

  describe user('auser') do
    it { should exist }
  end
end

control 'geany-config-file-geany-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/geany') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'geany-config-file-geany-config-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/geany/geany.conf') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
  end
end

control 'geany-config-file-geany-keybindings-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/geany/keybindings.conf') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
  end
end

control 'geany-config-file-geany-ui-toolbar-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/geany/ui_toolbar.xml') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
  end
end
