# frozen_string_literal: true

packages = %w[geany geany-plugins]

packages.each do |pkg|
  control "geany-package-clean-#{pkg}-pkg-removed" do
    title 'should not be installed'

    describe package(pkg) do
      it { should_not be_installed }
    end
  end
end
